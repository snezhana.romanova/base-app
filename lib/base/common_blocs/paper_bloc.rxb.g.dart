// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'paper_bloc.dart';

/// PaperBlocType class used for bloc event and state access from widgets
/// {@nodoc}
abstract class PaperBlocType extends RxBlocTypeBase {
  PaperBlocEvents get events;

  PaperBlocStates get states;
}

/// $PaperBloc class - extended by the PaperBloc bloc
/// {@nodoc}
abstract class $PaperBloc extends RxBlocBase
    implements PaperBlocEvents, PaperBlocStates, PaperBlocType {
  ///region Events

  ///region fetch

  final _$fetchEvent = PublishSubject<void>();
  @override
  void fetch() => _$fetchEvent.add(null);

  ///endregion fetch

  ///endregion Events

  ///region States

  ///region allPapers
  Stream<List<Paper>> _allPapersState;

  @override
  Stream<List<Paper>> get allPapers =>
      _allPapersState ??= _mapToAllPapersState();

  Stream<List<Paper>> _mapToAllPapersState();

  ///endregion allPapers

  ///endregion States

  ///region Type

  @override
  PaperBlocEvents get events => this;

  @override
  PaperBlocStates get states => this;

  ///endregion Type

  /// Dispose of all the opened streams

  @override
  void dispose() {
    _$fetchEvent.close();
    super.dispose();
  }
}
