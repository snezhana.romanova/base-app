// used sample: https://github.com/Prime-Holding/rx_bloc/blob/master/examples/news/lib/bloc/news_bloc.dart

import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';


import '../models/paper.dart';
import '../repositories/paper_repository.dart';

part 'paper_bloc.rxb.g.dart';

abstract class PaperBlocEvents{
  void fetch();
}

abstract class PaperBlocStates{
  Stream<List<Paper>> get allPapers;

  @RxBlocIgnoreState()
  Stream<bool> get isLoading;

  @RxBlocIgnoreState()
  Stream<String> get errors;
}

@RxBloc()
class PaperBloc extends $PaperBloc{
  PaperRepository _repository;

  PaperBloc(PaperRepository paperRepository);

  @override
  Stream<List<Paper>> _mapToAllPapersState() =>
      _$fetchEvent //auto generated subject
          .switchMap((_) => _repository.fetchAllPapers().asResultStream()) // fetch news
          .setResultStateHandler(this) // set handlers for loading/error states
          .whereSuccess();

  @override
  // TODO: implement errors
  Stream<String> get errors  => errorState.map((error) => error.toString());

  @override
  // TODO: implement isLoading
  Stream<bool> get isLoading => loadingState;
}

