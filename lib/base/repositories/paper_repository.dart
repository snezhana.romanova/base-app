import 'dart:async';

import '../../base/repositories/paper_api_provider.dart';
import '../models/paper.dart';

class PaperRepository {
  final reportApiProvider = PaperApiProvider();
  Future<List<Paper>> fetchAllPapers() => reportApiProvider.fetchPapers();
}
