import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../models/paper.dart';

class PaperApiProvider with ChangeNotifier {
  // Client clien = Client();
  static const _url =
      'https://my-base-project-repo/papers.json';

  Future<List<Paper>> fetchPapers() async {
    final response = await http.get(_url);
    if (response.statusCode == 200) {
      List<Paper> reports = [];
      for (int i = 0; i < json
          .decode(response.body)
          .length; i++) {
        reports.add(Paper.fromJson(json.decode(response.body)[i]));
      }
      return reports;
    } else {
      throw Exception('Failed to load report');
    }
  }
}
