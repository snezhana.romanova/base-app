class Paper {
  String id;
  String color;
  double height;
  double width;

  Paper({this.id, this.color, this.height, this.width});

  Paper.fromJson(Map<String, dynamic> parsedJson)
    {
      id = parsedJson['id'];
      color = parsedJson['color'];
      height = parsedJson['height'];
      width = parsedJson['height '];
    }
}