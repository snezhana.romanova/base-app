import 'package:auto_route/auto_route_annotations.dart';
import '../../feature_splash/views/splash_page.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    MaterialRoute(page: SplashPage, initial: true),
  ],
)
class $MyRouter {}
