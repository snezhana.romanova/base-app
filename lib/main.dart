import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:my_town_app/base/common_blocs/paper_bloc.dart';
import 'package:my_town_app/base/repositories/paper_repository.dart';
import 'base/routers/router.gr.dart';

void main() {
  // Enable integration testing with the Flutter Driver extension.
  // See https://flutter.dev/testing/ for more info.
  // enableFlutterDriverExtension();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => RxBlocProvider<PaperBlocType>(
    create: (_) => PaperBloc(PaperRepository()),
    child: MaterialApp(
      title: 'Base app',
      theme: ThemeData.light(),
      builder: ExtendedNavigator<MyRouter>(
        router: MyRouter(),
      ),
    ),
  );
}
